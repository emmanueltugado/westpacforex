//
//  Theme.swift
//  WestpacForex
//
//  Created by Emmanuel Tugado on 29/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import UIKit

enum Theme: String {
    case westpac, saintGeorge

    var primaryColor: UIColor {
        switch self {
        case .westpac: return UIColor(hexString: "#FF6666")
        case .saintGeorge: return UIColor(hexString: "#66FF99")
        }
    }

    var primaryFontName: String {
        switch self {
        case .westpac: return "KohinoorBangla-Regular"
        case .saintGeorge: return "TrebuchetMS"
        }
    }
}
