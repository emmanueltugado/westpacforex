//
//  ThemeManager.swift
//  WestpacForex
//
//  Created by Emmanuel Tugado on 29/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import UIKit

protocol ThemeHandler {
    static var currentTheme: Theme { get }
    static func setAsCurrentTheme(_ theme: Theme)
}

struct ThemeManager {
    private static let currThemeKey = "currentTheme"

    static var currentTheme: Theme {
        guard let themeRaw = UserDefaults.standard.string(forKey: ThemeManager.currThemeKey),
            let theme = Theme(rawValue: themeRaw)
        else { return .westpac}

        return theme
    }

    static func setAsCurrentTheme(_ theme: Theme) {
        UserDefaults.standard.set(theme.rawValue, forKey: currThemeKey)
    }
}
