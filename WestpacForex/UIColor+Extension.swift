//
//  UIColor+Extension.swift
//  Nicehash Monitor
//
//  Created by Emmanuel Francisco Tugado on 18/8/17.
//  Copyright © 2017 Emmanuel Tugado. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hexString: String) {
        let nonHexChars = NSCharacterSet(charactersIn: "#0123456789ABCDEFabcdef").inverted

        if  hexString.count > 7 || hexString.count < 6 ||
            hexString.rangeOfCharacter(from: nonHexChars) != nil {
            
            self.init(red: 0, green: 0, blue: 0, alpha: 0)
            
        } else {
            var hex: Int
            
            if hexString.hasPrefix("#") {
                let secondChar = hexString.index(hexString.startIndex, offsetBy: 1)
                hex = Int(hexString[secondChar...], radix: 16)!
            } else {
                hex = Int(hexString, radix: 16)!
            }
            
            let red = CGFloat((hex & 0xFF0000) >> 16) / 255
            let green = CGFloat((hex & 0x00FF00) >> 8) / 255
            let blue = CGFloat(hex & 0x0000FF) / 255
            
            self.init(red: red, green: green, blue: blue, alpha: 1)
        }
    }
}
