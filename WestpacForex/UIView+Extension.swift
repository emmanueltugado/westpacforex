//
//  UIView+Extension.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 28/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import UIKit

extension UIView {
    func addWithoutAutoresizingMask(view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
}
