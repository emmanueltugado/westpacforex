//
//  ForexViewController.swift
//  RestClient
//
//  Created by Emmanuel Tugado on 28/11/18.
//  Copyright © 2018 Emmanuel Tugado. All rights reserved.
//

import UIKit
import ForexFramework

class ForexViewController: UIViewController {
    lazy var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 2

        return formatter
    }()

    lazy var audTextField: UITextField = {
        let tf = UITextField()
        tf.keyboardType = .decimalPad
        tf.textAlignment = .right
        tf.delegate = self
        tf.placeholder = "0.0"
        tf.font = UIFont(name: ThemeManager.currentTheme.primaryFontName, size: 34)
        tf.addTarget(self, action: #selector(performConversion), for: .editingChanged)

        return tf
    }()

    lazy var audLabel: UILabel = {
        let label = UILabel()
        label.text = "AUD"
        label.font = UIFont(name: ThemeManager.currentTheme.primaryFontName, size: 30)
        label.textColor = UIColor.black

        return label
    }()

    lazy var isLabel: UILabel = {
        let label = UILabel()
        label.text = "IS"
        label.font = UIFont(name: ThemeManager.currentTheme.primaryFontName, size: 30)
        label.textColor = UIColor.white

        return label
    }()

    lazy var forexLabel: UILabel = {
        let label = UILabel()
        label.text = "0.0"
        label.font = UIFont(name: ThemeManager.currentTheme.primaryFontName, size: 32)
        label.textColor = UIColor.black

        return label
    }()

    lazy var forexButton: UIButton = {
        let button = UIButton(type: .system)

        if let code = viewModel.allTradingCurrencies.first?.code.uppercased() {
            button.setTitle(code, for: .normal)
        }

        button.titleLabel?.font = UIFont(name: ThemeManager.currentTheme.primaryFontName, size: 30)
        button.addTarget(self, action: #selector(showCurrencyPicker), for: .touchUpInside)

        return button
    }()

    lazy var forexStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [forexLabel, forexButton])
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.alignment = .lastBaseline

        return stackView
    }()

    lazy var audStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [audTextField, audLabel])
        stackView.axis = .horizontal
        stackView.spacing = 16
        stackView.alignment = .lastBaseline

        return stackView
    }()

    lazy var fullStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [audStack, isLabel, forexStack])
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.alignment = .center

        return stackView
    }()

    lazy var backGroundTap: UITapGestureRecognizer = {
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(endAllEditing))
        tapGest.numberOfTapsRequired = 1

        return tapGest
    }()

    lazy var pickerView: UIPickerView = {
        let picker = UIPickerView()
        picker.dataSource = self
        picker.delegate = self

        picker.isHidden = true

        return picker
    }()

    lazy var quoteTimer: Timer = {
        return Timer(timeInterval: 90, target: self, selector: #selector(fetchRates), userInfo: nil, repeats: true)
    }()

    let viewModel = ForexViewModel()
    let currentFontName = ThemeManager.currentTheme.primaryFontName

    internal(set) var tradingCurrency: TradingCurrency?

    override func viewDidLoad() {
        super.viewDidLoad()

        quoteTimer.fire()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        audTextField.becomeFirstResponder()

        super.viewWillAppear(animated)
    }

    private func setupUI() {
        view.backgroundColor = ThemeManager.currentTheme.primaryColor
        
        view.addGestureRecognizer(backGroundTap)
        view.addWithoutAutoresizingMask(view: fullStack)
        view.addWithoutAutoresizingMask(view: pickerView)

        fullStack.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor).isActive = true
        fullStack.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor, constant: -64).isActive = true

        pickerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        pickerView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pickerView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }

    @objc private func fetchRates() {
        viewModel.getConversionRates { _ in
            if self.forexButton.title(for: .normal) == nil,
                let code = self.viewModel.allTradingCurrencies.first?.code.uppercased() {
                self.forexButton.setTitle(code, for: .normal)
                self.tradingCurrency = self.viewModel.currencyWithCode(code)
            }

            self.pickerView.reloadAllComponents()
        }
    }

    @objc func performConversion() {
        guard let audText = audTextField.text,
            audText != ""
            else {
                forexLabel.text = "0.0"
                return
        }

        guard let currency = tradingCurrency,
            let aud = Double(audText)
            else { return }

        let conversion = viewModel.convertFromAUD(to: currency, amount: aud)

        if let converted = numberFormatter.string(from: NSNumber(value: conversion)) {
            forexLabel.text = converted
        }
    }

    @objc private func endAllEditing() {
        view.endEditing(true)
        pickerView.isHidden = true
    }

    @objc private func showCurrencyPicker() {
        pickerView.isHidden = false
    }
}
